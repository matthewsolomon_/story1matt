from django.shortcuts import render

# Create your views here.
# To view page to html
def index(request):
    return render(request, 'home/index.html')